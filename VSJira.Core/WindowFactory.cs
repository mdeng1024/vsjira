﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using VSJira.Core.UI;

namespace VSJira.Core
{
    public class WindowFactory : IWindowFactory
    {
        private readonly Dictionary<Type, Func<object, Window>> _factoryMap = new Dictionary<Type, Func<object, Window>>()
        {
            { typeof(ConnectDialogViewModel), (vm) => new ConnectDialog((ConnectDialogViewModel)vm) },
            { typeof(TransitionIssueDialogViewModel), (vm) => new TransitionIssueDialog((TransitionIssueDialogViewModel)vm) },
            { typeof(CreateIssueDialogViewModel), (vm) => new CreateIssueDialog((CreateIssueDialogViewModel)vm) },
            { typeof(OpenIssueDialogViewModel), (vm) => new OpenIssueDialog((OpenIssueDialogViewModel)vm) },
            { typeof(TracesDialogViewModel), (vm) => new TracesDialog((TracesDialogViewModel)vm) },
        };

        private Window CreateWindowForViewModel(object viewModel)
        {
            var window = _factoryMap[viewModel.GetType()].Invoke(viewModel);
            window.DataContext = viewModel;
            return window;
        }

        public void Show(object viewModel)
        {
            CreateWindowForViewModel(viewModel).Show();
        }

        public void ShowDialog(object viewModel)
        {
            CreateWindowForViewModel(viewModel).ShowDialog();
        }

        public void ShowMessageBox(string text, Exception ex = null)
        {
            if (ex != null)
            {
                text = $"{text} Details: {ex.Message} (Exception details have been copied to your clipboard)";
                Clipboard.SetText(ex.ToString());
            }

            MessageBox.Show(text);
        }
    }
}
