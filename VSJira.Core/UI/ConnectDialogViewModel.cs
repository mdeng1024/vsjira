﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;

namespace VSJira.Core.UI
{
    public class ConnectDialogViewModel : ViewModelBase
    {
        public class RequestPasswordEventArgs : EventArgs
        {
            public string Password { get; set; }
        }

        private readonly TraceListener _traceListener;
        private IJiraOptions _options;
        private string _url;
        private string _username;
        private string _status;
        private Exception _exception;
        private JiraServerInfo _selectedServer;
        private bool _isUrlFocused;
        private bool _isPasswordFocused;

        public DialogResult Result { get; private set; }
        public ObservableCollection<JiraServerInfo> Servers { get; private set; }
        public event EventHandler<RequestPasswordEventArgs> RequestingPassword;
        public DelegateCommand<ICloseableWindow> ConnectCommand { get; private set; }
        public DelegateCommand CopyExceptionCommand { get; private set; }
        public DelegateCommand<ICloseableWindow> ShowOptionsPageCommand { get; private set; }

        public class DialogResult
        {
            public Jira Jira { get; set; }
            public string Username { get; set; }
        }

        public ConnectDialogViewModel(IJiraFactory jiraFactory, VSJiraServices services, TraceListener traceListener)
        {
            this.Services = services;
            this._traceListener = traceListener;
            this._options = this.Services.VisualStudioServices.GetConfigurationOptions();

            this.Result = new DialogResult();
            this.Status = "Ready.";
            this.Exception = null;
            this.ConnectCommand = new DelegateCommand<ICloseableWindow>(async (window) => await this.ConnectToJira(window));
            this.ShowOptionsPageCommand = new DelegateCommand<ICloseableWindow>((window) => this.ShowOptionsPage(window));
            this.CopyExceptionCommand = new DelegateCommand(() => this.CopyException());
            this.Servers = new ObservableCollection<JiraServerInfo>(_options.JiraServers);
            this.SelectedServer = this.Servers.FirstOrDefault();
        }

        public VSJiraServices Services { get; }

        public bool IsUrlFocused
        {
            get { return _isUrlFocused; }
            set
            {
                _isUrlFocused = value;
                OnPropertyChanged("IsUrlFocused");
            }
        }

        public bool IsPasswordFocused
        {
            get { return _isPasswordFocused; }
            set
            {
                _isPasswordFocused = value;
                OnPropertyChanged("IsPasswordFocused");
            }
        }

        public JiraServerInfo SelectedServer
        {
            get { return _selectedServer; }
            set
            {
                _selectedServer = value;
                OnPropertyChanged("SelectedServer");
                PopulateFieldsFromSettings();

                this.IsUrlFocused = false;
                this.IsPasswordFocused = false;

                if (_selectedServer == null)
                {
                    this.IsUrlFocused = true;
                }
                else
                {
                    this.IsPasswordFocused = true;
                }
            }
        }

        public string Status
        {
            get { return _status; }
            set
            {
                _status = value;
                OnPropertyChanged("Status");
            }
        }

        public Exception Exception
        {
            get
            {
                return _exception;
            }
            set
            {
                _exception = value;
                OnPropertyChanged("Exception");
            }
        }

        public string Url
        {
            get { return _url; }
            set
            {
                _url = value.Trim();
                OnPropertyChanged("Url");
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value.Trim();
                OnPropertyChanged("Username");
            }
        }

        private void PopulateFieldsFromSettings()
        {
            if (this.SelectedServer != null)
            {
                this.Url = this.SelectedServer.Url;
                this.Username = this.SelectedServer.Username;
            }
        }

        private Jira CreateJira()
        {
            try
            {
                var passwordArgs = new RequestPasswordEventArgs();
                this.OnRequestPassword(passwordArgs);
                var jira = this.Services.JiraFactory.Create(
                    this.Url,
                    this.Username,
                    passwordArgs.Password);

                if (jira.RestClient != null && jira.RestClient.RestSharpClient != null
                    && this.SelectedServer != null && !String.IsNullOrWhiteSpace(this.SelectedServer.ProxyUrl))
                {
                    jira.RestClient.RestSharpClient.Proxy = new System.Net.WebProxy(this.SelectedServer.ProxyUrl);
                }

                Trace.Listeners.Remove(_traceListener);
                if (_options.EnableRequestTracing)
                {
                    jira.RestClient.Settings.EnableRequestTrace = true;
                    Trace.Listeners.Add(_traceListener);
                }
                return jira;

            }
            catch (Exception ex)
            {
                this.Status = "Error: Please provide valid values for all fields.";
                this.Exception = ex;
                return null;
            }
        }

        private Task PrepareJiraClient(Jira jira)
        {
            return Task.WhenAll(
                jira.Fields.GetCustomFieldsAsync(),
                jira.Priorities.GetPrioritiesAsync(),
                jira.Statuses.GetStatusesAsync(),
                jira.IssueTypes.GetIssueTypesAsync(),
                jira.Resolutions.GetResolutionsAsync()
           );
        }

        private void CopyException()
        {
            if (this.Exception != null)
            {
                Clipboard.SetText(this.Exception.ToString());
            }
        }

        private async Task ConnectToJira(ICloseableWindow window)
        {
            this.Status = "Connecting...";
            this.Exception = null;

            var jira = CreateJira();

            if (jira != null)
            {
                try
                {
                    await PrepareJiraClient(jira);
                    this.Result.Username = this.Username;
                    this.Result.Jira = jira;
                    window.Close();
                }
                catch (Exception ex)
                {
                    this.Status = "Error: " + ex.Message;
                    this.Exception = ex;
                }
            }
        }

        private void ShowOptionsPage(ICloseableWindow window)
        {
            window.Close();
            this.Services.VisualStudioServices.ShowOptionsPage();
        }

        /// <summary>
        /// This is a work around to get the password from view given that the
        /// PasswordBox is not bindable
        /// </summary>
        private void OnRequestPassword(RequestPasswordEventArgs args)
        {
            var handler = this.RequestingPassword;
            if (handler != null)
            {
                handler(this, args);
            }
        }
    }
}
