﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VSJira.Core.UI
{
    public class TracesDialogViewModel : JiraBackedViewModel
    {
        public string Traces { get; set; }

        public TracesDialogViewModel(VSJiraServices services)
            : base(services)
        {
        }
    }
}
