﻿using Atlassian.Jira;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace VSJira.Core.UI
{
    /// <summary>
    /// Interaction logic for ConnectDialog.xaml
    /// </summary>
    public partial class ConnectDialog : MetroWindow, ICloseableWindow
    {
        public ConnectDialog(ConnectDialogViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            viewModel.RequestingPassword += viewModel_RequestPassword;

            var theme = viewModel.Services.VisualStudioServices.GetConfigurationOptions().Theme;
            ThemeManager.Apply(this.Resources, theme);
        }

        void viewModel_RequestPassword(object sender, ConnectDialogViewModel.RequestPasswordEventArgs e)
        {
            e.Password = this.UserPassword.Password;
        }
    }
}
