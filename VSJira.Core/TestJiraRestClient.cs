﻿using Atlassian.Jira.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Atlassian.Jira;
using Newtonsoft.Json.Linq;
using RestSharp;
using System.Threading;

namespace VSJira.Core
{
    public class TestJiraRestClient : IJiraRestClient
    {
        public TestJiraRestClient()
        {
            this.Settings = new JiraRestClientSettings();
        }

        public RestClient RestSharpClient
        {
            get
            {
                return null;
            }
        }

        public JiraRestClientSettings Settings
        {
            get;
            private set;
        }

        public string Url
        {
            get
            {
                return "http://testjira";
            }
        }

        public void Download(string url, string fullFileName)
        {
            throw new NotImplementedException();
        }

        public byte[] DownloadData(string url)
        {
            throw new NotImplementedException();
        }

        public Task<IRestResponse> ExecuteRequestAsync(IRestRequest request, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<JToken> ExecuteRequestAsync(Method method, string resource, object requestBody = null, CancellationToken toke = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<T> ExecuteRequestAsync<T>(Method method, string resource, object requestBody = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }
    }
}
