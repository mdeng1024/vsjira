﻿using Atlassian.Jira;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using Atlassian.Jira.Remote;
using System.Diagnostics;

namespace VSJira.Core
{
    public class TestJiraServices :
        IIssueFieldService,
        IIssueStatusService,
        IIssuePriorityService,
        IIssueTypeService,
        IIssueResolutionService,
        IProjectService,
        IIssueFilterService,
        IProjectComponentService,
        IProjectVersionService
    {
        private readonly Jira _jira;

        private string[] _jiraProjects = new string[2] { "Test", "Prod" };
        private string[] _jiraStatuses = new string[2] { "Open", "In Progress" };
        private string[] _jiraPriorities = new string[2] { "High", "Low" };
        private string[] _jiraTypes = new string[2] { "Bug", "Feature" };
        private string[] _jiraResolutions = new string[2] { "Fixed", "Duplicate" };
        private string[] _jiraFilters = new string[2] { "My Open Issues", "Assigned To Me" };
        private string[] _jiraComponents = new string[2] { "Back-end", "Front-end" };
        private string[] _jiraVersions = new string[2] { "v1.0", "v2.0" };

        public TestJiraServices(Jira jira)
        {
            _jira = jira;
        }

        public Task<IEnumerable<CustomField>> GetCustomFieldsAsync(CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetCustomFieldsAsync");

            return Task.FromResult(Enumerable.Empty<CustomField>());
        }

        public Task<IEnumerable<IssueType>> GetIssueTypesAsync(CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetIssueTypesAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssueType(new RemoteIssueType()
                {
                    id = i.ToString(),
                    name = _jiraTypes[i]
                })));
        }

        public Task<IEnumerable<IssueType>> GetIssueTypesForProjectAsync(string projectKey, CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetIssueTypesForProjectAsync");

            return this.GetIssueTypesAsync(token);
        }

        public Task<IEnumerable<IssuePriority>> GetPrioritiesAsync(CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetPrioritiesAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssuePriority(new RemotePriority()
                {
                    id = i.ToString(),
                    name = _jiraPriorities[i]
                })));
        }

        public Task<IEnumerable<IssueStatus>> GetStatusesAsync(CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetStatusesAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssueStatus(new RemoteStatus()
                {
                    id = i.ToString(),
                    name = _jiraStatuses[i]
                })));
        }

        public Task<IEnumerable<IssueResolution>> GetResolutionsAsync(CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetResolutionsAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new IssueResolution(new RemoteNamedObject()
                {
                    id = i.ToString(),
                    name = _jiraResolutions[i]
                })));
        }

        public Task<IEnumerable<Project>> GetProjectsAsync(CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetProjectsAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new Project(_jira, new RemoteProject()
                {
                    id = i.ToString(),
                    name = _jiraProjects[i],
                    key = _jiraProjects[i].ToUpperInvariant()
                })));
        }

        public Task<Project> GetProjectAsync(string projectKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<JiraFilter>> GetFavouritesAsync(CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetFavouritesAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new JiraFilter(i.ToString(), _jiraFilters[i], _jiraFilters[i])
            ));
        }

        public Task<IPagedQueryResult<Issue>> GetIssuesFromFavoriteAsync(string filterName, int? maxIssues = default(int?), int startAt = 0, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<ProjectComponent> CreateComponentAsync(ProjectComponentCreationInfo projectComponent, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task DeleteComponentAsync(string componentId, string moveIssuesTo = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ProjectComponent>> GetComponentsAsync(string projectKey, CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetComponentsAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new ProjectComponent(new RemoteComponent() { id = i.ToString(), name = _jiraComponents[i], ProjectKey = projectKey })
            ));
        }

        public Task<ProjectVersion> CreateVersionAsync(ProjectVersionCreationInfo projectVersion, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task DeleteVersionAsync(string versionId, string moveFixIssuesTo = null, string moveAffectedIssuesTo = null, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<ProjectVersion>> GetVersionsAsync(string projectKey, CancellationToken token = default(CancellationToken))
        {
            Trace.WriteLine("TestJira GetVersionsAsync");

            return Task.FromResult(Enumerable.Range(0, 2).Select(i =>
                new ProjectVersion(_jira, new RemoteVersion() { id = i.ToString(), name = _jiraVersions[i], ProjectKey = projectKey })
            ));
        }

        public Task<ProjectVersion> GetVersionAsync(string versionId, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<ProjectVersion> UpdateVersionAsync(ProjectVersion version, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IPagedQueryResult<ProjectVersion>> GetPagedVersionsAsync(string projectKey, int startAt = 0, int maxResults = 50, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CustomField>> GetCustomFieldsForProjectAsync(string projectKey, CancellationToken token = default(CancellationToken))
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<CustomField>> GetCustomFieldsAsync(CustomFieldFetchOptions options, CancellationToken token = default)
        {
            throw new NotImplementedException();
        }
    }
}
